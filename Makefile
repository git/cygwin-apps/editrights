srcdir=.
VPATH=${srcdir}

DESTDIR=
prefix=/usr
bindir=$(prefix)/bin
docdir=$(prefix)/share/doc/Cygwin

EXEEXT=.exe
OBJ=	main.o \
	printntstatus.o

editrights$(EXEEXT): $(OBJ)
	$(CC) $(LDFLAGS) -o $@ $(OBJ)

install: editrights$(EXEEXT)
	-mkdir -p $(DESTDIR)$(bindir)
	install -m 755 editrights$(EXEEXT) $(DESTDIR)$(bindir)/editrights$(EXEEXT)
	-mkdir -p $(DESTDIR)$(docdir)
	install -m 644 $(srcdir)/editrights.README $(DESTDIR)$(docdir)/editrights.README

clean:
	rm -f editrights$(EXEEXT) core editrights$(EXEEXT).stackdump *.o

#include <stdio.h>
#include <windows.h>
#include <ntsecapi.h>
#include <ntstatus.h>

void
printNTSTATUS (NTSTATUS err)
{
  switch (err)
    {
    case STATUS_SUCCESS:
      printf ("STATUS_SUCCESS");
      break;
    case STATUS_NONE_MAPPED:
      printf ("STATUS_NONE_MAPPED");
      break;
    case STATUS_NO_SUCH_PRIVILEGE:
      printf ("STATUS_NO_SUCH_PRIVILEGE");
      break;
    case RPC_NT_SERVER_UNAVAILABLE:
      printf ("RPC_NT_SERVER_UNAVAILABLE");
      break;
    case RPC_NT_SERVER_TOO_BUSY:
      printf ("RPC_NT_SERVER_TOO_BUSY");
      break;
    default:
      printf ("unknown status code");
      break;
    }
}
